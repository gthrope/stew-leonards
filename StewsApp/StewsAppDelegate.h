//
//  StewsAppDelegate.h
//  StewsApp
//
//  Created by Glenn Thrope on 9/6/14.
//  Copyright (c) 2014 Stew Leonards. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StewsAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
