//
//  main.m
//  StewsApp
//
//  Created by Glenn Thrope on 9/6/14.
//  Copyright (c) 2014 Stew Leonards. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "StewsAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([StewsAppDelegate class]));
    }
}
